# Read Me

I believe that information should be free.

Continuous Credit to Sandler, Becc Holland, Tito Bohrt and all the others before me.

The reason I'm putting this together is to provide a bootstrap library of templates for a go-to market outbound motion. It's modular, customizable, and will work as a first draft from which you can iterate and tailor to your unique situation. This is not an end all be all, it is a starting point. 

It's also important to point out that I've worked in the context of an Enterprise Outbound motion. If you're dealing with inbound, SMB, Mid-Market, renewals, your context is different, so your mileage may vary.

Happy Iterating!

## Licensing

See the LICENSE file for licensing information as it pertains to files in this repository.

## Sorting 

Outbound is a sorting motion, not a selling motion. Given a set of x prospects and their contact information, only subset y will be actively in the market for a solution. Outbound/Demand Gen/Sales Development's responsibility is to find subset y and schedule an initial discovery call. That does not include selling. It's solely having a conversation to confirm that the prospect has relevant problems your product may be able to solve.

The question becomes, how do you quickly and efficiently identify the very small subset y out of sea of prospects x? At that point it comes down to volume of activity. Good news is as you process through your ICP list, your sawdust ends up as a cartography of the territory, and you end up with a top down snapshot of who's in the market, who's not ready, and who might be purchasing in the next 18 months. 

## Four Bottlenecks

There are four bottlenecks in outbound.

1. Contact Data - How easy is it to get the phone/email/contact info of your ICP? This bottleneck should be a non-issue when you have the correct tooling in place. If getting contact data takes too long, you're wasting time on the least valuable bottleneck.
2. Messaging - Is what you're saying relevant? You can go value prop based, which is what's easiest and what most companies do. However, if you know your ICP intimately and have predictive insights, it's far stronger to be able to talk about the problems they're likely encountering, and what will happen down the road as a result of it.
3. Channel - Where are you reaching them? Phone, Email, LinkedIn. There may be other channels where this prospect is active (Reddit, HackerNews, Twitter/Mastodon, etc.) Figure out where they prefer to be reached.
4. Timing. - Sometimes, you reach the right prospect, with the right message, on the right channel. But outside of your control, they're not in the market for your solution. No problem, that's when you add them to a quarterly follow-up to stay relevant and top of mind, should the time come that they are ready to evaluate products.

## Resources

* [Leads](https://docs.google.com/spreadsheets/d/1huOH4ev4z8P8HXqwRuJ87-M3ojxkkNgv/edit?usp=sharing&ouid=103605880697996511977&rtpof=true&sd=true)
* [Accounts / PhantomBuster](https://docs.google.com/spreadsheets/d/1huOH4ev4z8P8HXqwRuJ87-M3ojxkkNgv/edit?usp=sharing&ouid=103605880697996511977&rtpof=true&sd=true)
* [Deals](https://docs.google.com/spreadsheets/d/1LKo-Kpt6kecz24do1WNe9u1ll0Yd2m-5/edit?usp=sharing&ouid=103605880697996511977&rtpof=true&sd=true)
* Copy Swipe
* Call Script