# Call Script - Outbound


## HOOK  


    Hi, {{first_name}}, this is {{sdr_name}} with env0.


    I was hoping to speak with you briefly.


    Do you have 2 minutes? 


    **If “send me an email, or call me later”**


    Sounds like I caught you at a bad time. I’m happy to (call back later/send an email), but does it make sense to quickly tell you why I called so you can let me know if it’s relevant for you?


        **If “Yes” **→ Thanks, I’ll be brief. → Pitch


    **If “no” again** → send hook reject template

Hook Options:



1. Hi {{First_name}}, It’s {{name}}. I caught you at a bad time on {{_day_}}, do you have 2 minutes for me now?
    1. **If No ->** Okay, should I call back in an hour, or is tomorrow better, I just have 2 quick questions...
2. Hey {{First_name}}, this is {{name}} from {{company}}, I was on your LinkedIn profile and was hoping you could help me out . Do you have 2 minutes?
3. Hi {{First_name}}, this is {{name}} at {{company}}. \
 I was looking for a little bit of help. Do you have two minutes, so I can tell you why I called \
 and you can let me know if it makes sense for us to talk further?


**FlyUnder (Use with Gatekeepers)**


     Hi this is {{sdr_name}}  calling for {{first_name}}. Is s/he in? 


**Is this a sales call? **

Not looking to sell you anything. The reason I'm calling is because we're hosting executive webinars on infrastructure-as-code. I just had two quick questions to see if it would even be relevant to you. Is that fair?


    (If you do not get permission on hook, move to half-pitch)


    **Sorry, do we know each other?** 


    **Is this a sales call?** 


    **What’s env0? / About what?** 


    **Who is this?**


    

**Yes / Sure →  **Thanks, I’ll be brief.


## QUICK PITCH


    You might have heard of env0 from the Forrester review of Infrastructure-as-Code tools… The reason for my call is because we’re hosting personalized screen shares showing how companies like Paypal and MongoDB automate their IaC...


    I have 2 quick questions to see if this would be relevant…. Is that fair?   


    (Drop “Is it fair” straight to Q1 if they did not give permission for 2mins)


## GAP (Self Rating Version)


# **Q1:** On a scale of 1-10, one being you’re just starting to use Terraform, to ten meaning you’ve enabled developer self-service, how far along would you say are in your Infrastructure-as-Code journey?


# **Q2:** Makes sense, I hear that a lot. Since you mentioned you're at a (x), what would it take to get you to a 10?

**<span style="text-decoration:underline;">		{{ACTIVE LISTEN}}:</span>**  Awesome. I’m glad I gave you call, like I said I’ll keep this short.**[CLOSE]**


## QUICK PITCH - Land & Expand


    The reason I’m calling is because Eddie and Shaked from the BCDC group are using env0 to deploy their infrastructure-as-code. Are you familiar with the BCDC group?


    **Yes** -> Great to hear you’re familiar with them.


    **No** -> No worries. BCDC is Paypal’s digital asset and cryptocurrency organization. 


    I called because we’re hosting workshops to show the rest of the org how they can also leverage existing resources to automate, scale, and secure their deployments.


    I have 2 quick questions to see if this would be relevant…. Is that fair?   


## CLOSE


    Because you are {{what_they_said}}, I highly recommend you take a look at a personalized screen share. We’re sharing the journey that Paypal and MongoDB went through to automate their Infrastructure as Code. 


    (Push/Pull) Looking at calendars, this week’s pretty booked up already. But I may be able to squeeze in some time.... Do you have **15 min** Tuesday at 10:00 am or would some other time on Thursday work better for you?


## Post-Sell


    Great. I’ll send over a calendar invite to {{email}}, is that the best email for you? Please do me a favor and hit accept when you see it, that way I know it didn’t end up in spam. \



    Last question and I won’t hold you up any longer. I want to make sure this time is valuable for you. Is there anything that’s top of mind that you want to make sure we cover in that call? Typically, the DevOps teams we talk to are thinking about automation, governance, self-service, ephemeral environments, drift detection, or cost monitoring. -> Perfect, you’re going to love this then. We’ll be sure to cover that.


## Objection Handling


    **Not Buying Anything**


    Not looking to ask you to buy anything, or to rip-and-replace. This can be a purely informational session, even if it’s for down-the-road. What does your calendar look like for x?


    **CI/GitHub Actions/GitLab CI/Jenkins/Harness/Ansible Tower**


    Glad to hear that’s working well for you. We have customers that use GitHub/Jenkins{etc.} and env0. A couple of key patterns we’ve seen from those customers.  \
 \
While {{CI Tool}} is great for code build, test, etc, it isn't purpose built for IaC lifecycle management. And because of that Day 2 operations end up being a bottleneck. Sure it can spin it up - but how do you go about maintaining those resources?


    (optional) and checking the environment, drift detection, cost monitoring, governance/compliance, module management, or even allowing self-service for dev productivity.


    Not asking you to rip-and-replace. Highly recommend we have a conversation, even if it’s for down-the-road considerations. What does your calendar look like for x?


    **Just send me an email**


    I have no problem sending over some information, however can I ask just one more question to make sure I’m sending you the right information and I’m not wasting your time?


    **If Yes -> **Are you finding that you and/or your team are able to focus more on mission critical processes or are you fighting fires and tied up with monotonous tasks?


    **Either Answer -> **Are you familiar with Zip - QuadPay? We cut the merge to meantime in half, from 2 days to 1 and increased velocity - total number of merges by 2x . Increasing productivity while staying safe with having governance and control.


    So how does (Day and Time) work for you? 


    **Why are you calling my cell phone?**


    I chose to call your cell phone rather than your office line because so many people are working from home these days → go back to script.  \
 \
alternatively \
 \
 My apologies. I had this listed as your work line. I'm happy to take this number off the list, but would it make sense for me to tell you quickly why I called and you can let me know if I should even try your office line or not? → go back to script. 


    **How did you get my number!?!?**


    I called because of your role in {{title}} and we research tools like LinkedIn, Zoominfo, and Lusha to find your contact info. → go back to script. 


    **Referral**


    I also have {{person1}} and {{person2}} as people who might be handling Terraform at {{company}}. Who do you think would benefit the most from having that conversation?


    **Send me an invite**


    Perfect. So we have something confirmed because I know calendars fill up fast. Does either (date&time1) or (date&time2) work better for you? 


    **I don’t think this is relevant/I’m not interested**


    I can appreciate that, I didn’t really expect for you to be interested cause you don’t have enough information on what we do yet. But what you may be interested in is streamlining automation, governance, and self service. 


    Which of those resonates most with your priorities? 


    **We’re not using Terraform / We’re not there yet/ My team doesn’t handle Terraform**


    (Not a fit. Improve lead targeting.)


    **Can you just send me a pre-recorded webinar?**


    Because every DevOps team has a different environment, we schedule time with you to tailor the insights and best practices.


# Call Script - Postbound/Inbound/Survey


## HOOK 


    Hi, {{first_name}}, this is {{sdr_name}} with env0.


    I was hoping to speak with you briefly.


    Do you have 2 minutes?

Hook Options:



1. Hi, {{first_name}}, this is {{sdr_name}} with env0. How have you been?
2. Hi {{First_name}}, It’s {{name}}. I caught you at a bad time on {{_day_}}, do you have 2 minutes for me now?
    1. **If No ->** Okay, should I call back in an hour, or is tomorrow better, I just have 2 quick questions...
3. Hey {{First_name}}, this is {{name}} from {{company}}, I was on your LinkedIn profile and was hoping you could help me out . Do you have 2 minutes?
4. Hi {{First_name}}, this is {{name}} at {{company}}. \
 I was looking for a little bit of help. Do you have two minutes, so I can tell you why I called \
 and you can let me know if it makes sense for us to talk further?

    **If “send me an email, or call me later”**


    Ok, seems like I caught you at a bad time. I’m happy to (call back later/send an email), but does it make sense to quickly tell you why I called so you can let me know if it’s relevant for you?


        **If “Yes” **→ Thanks, I’ll be brief. → Pitch


    **If “no” again** → send hook reject template


    (If you do not get permission on hook, move to half-pitch)


    **Sorry, do we know each other?** 


    **Is this a sales call?** 


    **What’s env0? / About what?** 


    **Who is this?**


    


**Yes / Sure →  **Thanks, I’ll be brief.


## QUICK PITCH


    Good to hear. I’ll keep this brief. The reason I’m calling is because {{premise}} (you had downloaded the IaC ebook/signed up for an account/filled out our survey) but more importantly {{personalized}} (you mentioned on LinkedIn that you use Terraform). 


    I just had two quick questions based on your answers. Is that fair?


## QUALIFIED PITCH


# **Q1:** For context, are you using Terraform, Terragrunt, Pulumi, Cloudformation or some other form of infrastructure-as-code?

 


    **<span style="text-decoration:underline;">If No -></span>** Ok, so I might’ve called the wrong person. I also have {{person1}} and {{person2}} as people who might be more familiar with Terraform at {{company}}. Who do you recommend I call?


    **<span style="text-decoration:underline;">If Yes -></span>** **(continue)**

**<span style="text-decoration:underline;">Q2: If Familiar →</span>** Great! As you are deploying your Terraform workflows today... Are you running it manually, do you have homegrown tooling, or is it part of your CI?



1. **<span style="text-decoration:underline;">Custom → </span>**Ok great, you’re going to love this. 
1. **<span style="text-decoration:underline;">→ </span>**Awesome…. You’re going to love this...we are showcasing env0, which is a self-service catalogue for your ephemeral environments..


## GAP (Self Rating Version)


# **Q1:** On a scale of 1-10, one being you’re just starting to use Terraform, to ten meaning you’ve enabled developers self-service, how far along would you say are in your Infrastructure-as-Code journey?


# **Q2:** Makes sense, I hear that a lot. Since you mentioned you're at a (x), what would it take to get you to a 10?


## CLOSE


    I promised I’d keep this call under 2 minutes. Do you have your calendar handy? We’re pretty booked up this week, but does Tuesday at 2pm or would another time Thursday better work for you?  \
 \
Let me make sure I got this straight. {{summarize}} (You’re using Terraform, you’re doing 100 deploys/applies per month, and drift detection is important at that scale.) Is there anything I missed? Anything that’s top of mind you want to make sure we cover on that call.


    Perfect. Tuesday at 2pm, I want to hear more about your team, what you’re working on, and if it makes sense, we’ll share how env0 can support your efforts. Please do me a favor and hit accept on the invite so I know it didn’t end up in spam.

 \


********


## Social Proof 

Customer Endorsements 



* Paypal
* MongoDB
* Virgin Media
* Pleo
* Varonis
* jFrog
* BigID
* eToro
* Zip.co
*  Bain & Company

**Fintech**

Pleo ("the competition of Brex"), Zip ("the competition of Affirm"), PayPal obviously, eToro ("the competition of Robinhood") and also a European bank (Millenium).

**DevOps** 

JFrog, MongoDB and Gremlin makes sense

**Cyber** 

use Varonis, Wiz, Salt Security and BigID


## UP-FRONT CONTRACT



1. Why don’t we do this. Let me take 30 seconds, put some context around the conversation, and then you can let me know if it makes sense to explore further from there. Sound good?
2. I have 2 quick questions to see if this would be relevant…. Is that fair?   (Experiment with dropping “Is it fair” straight to Q1)


## ELEVATOR PITCH



1. 30 Sec Commercial

    Correct me if I’m wrong, but at the end of the day {{role}} care about {{personalize_to_their_LI_keywords}}.


    We typically work with {{title}} who want to automate their Terraform, but are still finding it full of manual work, lack of auditability, or lack of consistency. Or maybe they’re trying to establish guardrails and governance with granular RBAC. And the most advanced are moving towards a self-service model to empower their teams to provision on-demand.


    Are you running into this in your day-to-day, or am I just rambling here?

2. 2 Question Fail Safe

    Let me ask you two quick questions, and if I’m still not relevant after that, you won’t hear from me again. Sound good?

1. Closed Situational Question
    1. Correct me if I’m wrong but you’re using X. 
        1. In round numbers, How often would you say you’re running Terraform plan/apply? Do they need to run in parallel? If so, how many concurrently? 
        2. How many state files are you managing? How many workspaces do you require? How big is your application? How many modules?
2. Open Pain Question
    2. How are you detecting and managing drift, and ensuring that what’s defined is actually live in your infrastructure? 
    3. How are you tracking and managing costs of the resources that have been spun up?

****

 



1. **<span style="text-decoration:underline;">If 9-10 -></span>** Great! Usually when DevOps teams are at a 10, they’ve already got automation in place, have created governance and guardrails, and maybe even allow for developer self-service. Since you’re already at a 10, the insights we’re sharing will make perfect sense to you.
2. **<span style="text-decoration:underline;">If 7-8 -> Great! Usually when DevOps teams rate themselves at a {{7-8}}, they’ve implemented some automation, and the next steps they’re looking at would be to create governance/guardrails.</span>**
3. **<span style="text-decoration:underline;">If 1-6 -> Makes sense, we hear that a lot. Usually when DevOps teams are early in their IaC journey, they’re thinking about how to enable collaboration across devs as they scale their TF usage.</span>**

**<span style="text-decoration:underline;">Q2: Given the current situation, how are you handling Day 2 operations, or maintaining those resources, such as drift detection, cost monitoring, module management, etc.</span>**


# Structure 


    Hook


    Quick Pitch 


    GAP Q1-> Q2 (Qualified Pitch)


    Close


    Post Sale

By structuring our calls this way, we can have granular visibility into which areas may need troubleshooting as new reps onboard. For example, if we’re getting stuck on the Hook, that often means a change in tonality, while low conversions to a Qualified Pitch means that the value prop in the Quick Pitch needs to be reworked. No shows = be religious with the post-sell. Close Rejects = there’s no GAP, so either find another pain point, or fix prospect targeting.

 \



## Troubleshooting


    **Hook Reject**


    Listen for tonality and pacing.


    **Quick Pitch**


    Check that you’re following script. If yes, manager needs to listen for where we lose the prospect and fix value prop, possibly shorten length. Also possible that this is high because we’re not getting permission on hook.


    **Qualified Pitch**


    Listen for messaging fit. It becomes a Leads/Data team problem here, often fails because they don’t fit ICP.


    **Close Reject**


    Listen for Push/Pull. [Negative Reverse Sell](https://learn.sandler.com/mod/page/view.php?id=15001)


     \
 \

