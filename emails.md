# KMAC - Bootstrap any GTM with these templates

1. Outbound
	1. Main 
	2. Email Only
	3. Call Only
2. Postbound
	1. App Sign Up
	2. Content Download
3. Bridgebound
    1. Closed Lost Re-engage
    2. Deals Gone Dark
4. Field / Events
	1. Thanks for visiting our booth
5. Ancillary
	1. Demo Confirmation
	2. We Just Spoke
	3. Referral
	4. No Show
	5. 5 min reminder
	6. 24hr invite not accepted
6. Nurture
	1. Onboarding Dropoff
	2. Previous Usage
	3. Various Nurture Content


## 1 - Outbound

### Philosophically

You can go value prop based, which is what's easiest and what most companies do.

However, if you know your ICP intimately and have predictive insights, it's far stronger to be able to talk about the problems they're likely encountering, and what will happen down the road as a result of it.

In that case, I would focus on illuminating one of the common bottlenecks/problems, so that if they have one of these, they’re qualified and compelled to take a meeting.


### Outreach Settings

Day 1 - Call, leave VM
Day 1 - Email 1
Day 2 - Call
Day 8 - Call
Day 8 - Email 2
Day 9 - Call
Day 13 - Call
Day 13 - Email 3
Day 14 - Call
Day 18 - Call
Day 18 - Email 4
Day 19 - Call
Day 20 - Call
Day 21 - Call, leave VM
Day 21 - Email 5

Trigger - (Call+VM+Email) anytime you get 3+ views on an email, a profile view

### Main

#### Email 1

Hi {{first_name}},

Because you mentioned using Terraform on your LinkedIn, I thought this would be relevant to you.

Typically, the {{title}} I speak with tell me that they’ve done a good job building out Terraform, but they’re still finding a lot of manual work (from copying scripts, adding variables, rotating keys, etc.), and this means it’s difficult to scale the setup. (So they look TF Cloud / TF Enterprise, but find that the cost/pricing to feature set is ludicrous.)

Is this happening in your world at all, or worth even a brief conversation?

Either way, hope all is well in your world.

Warm Regards,
Kevin


#### Email 2 - reply 

any thoughts?

P.S. We’ve heard from users that “maintaining tags across your application is hard, especially when done manually”. So here’s an open source CLI tool for applying tags across your TF files and resources. 

#### Email 3

subj: actionable insights 
Hello {{first_name}},

{{!premise}} Because you said on LinkedIn that… I thought this would be of interest to you.

We surveyed 125 IaC practitioners, and 30% responded that they use a CI tool like Jenkins to execute Terraform “apply/plan/destroy” commands. 

But because CI pipelines aren't purpose built for IaC deployments, Day 2 operations end up becoming a bottleneck. (Sure it can spin up an EC2 instance - but how do maintain those resources?)

Do you have 15 mins to learn about how to solve the problems that come up with using a CI tool for your IaC deployments?

Warm Regards, 
{{sender.first_name}}

#### Email 4
subj: {{first name}} - drift detection
Correct me if I’m wrong, but at the end of the day {{title}} care about drift.
When it comes to managing states at scale, it can be really difficult to keep track of what’s been modified or what’s live in your infrastructure. And that’s why it’s so important to have a way to know when resources have been changed or need to be terminated.
We’re sharing insights on how companies like Paypal and JFrog have implemented drift detection. Do you have 15 mins to learn more?
Warm Regards,
 {{sender.first_name}}
P.S. Here’s our datasheet that covers drift detection, Plan on PR, and self-service.

#### Email 5

subj: appropriate person or (send as a reply/bump)

{{first_name}} - I was hoping to reach the person who's looking after your Infrastructure-as-Code. 

If it’s somebody else I should chat with, would you be so kind as to point me in the right direction please?

Thank you, 
{{sender.first_name}}

#### Email 6 - Fall on your sword lite

Hello {{first_name}},
I was rethinking my last email, and wanted to make sure I didn't overstep my bounds.
The reason I thought you might find value in having a discussion is that {{!premise} With your permission, I'd like another shot to earn some of your time.
We typically work with {{title}} who want to automate their Terraform, but are still finding it full of manual work, lack of auditability, or lack of consistency. Or maybe they’re trying to establish guardrails and governance with RBAC and OPA. And the most advanced are moving towards a self-service model to empower their teams to provision on-demand.
Are you running into this in your day-to-day, or am I just rambling here?
Warm Regards, 
{{sender.first_name}}

#### Email 7 - swan song

subj: swan song
Hello {{first_name}},
Given your {{!relevant_research}}, I initially thought it made sense to reach out.
Maybe it's me, but I'm getting the feeling that now is not the best time to connect.... Did I get that right?
If so, no worries, perhaps we can our paths will cross in the future.
All the best, 
{{sender.first_name}}

P.S. You might want to check out a few minutes from this talk. It points out the challenges of running Terraform as part of your CI (like the synchronisation problem)

### Email only

Clone main sequence, but remove all calls. For contacts without phone numbers, contacts in a different geographic region (e.g. You're in US, but they're in EMEA and time zones won't line up for calls.)

### Call only

Clone main sequence, but remove all emails. Sometimes the email bounces, and your best bet is to try to call.


## 2 - Postbound

Didn't directly book a demo, but may have downloaded some content or engaged with your app. Share insights, pitfalls, best practices. Nurture with content.

Follow up with various nurture style emails.

#### Email 1

sub: Thanks for {{!checking out env0/downloading our ebook/filling out the survey/etc.}}

Hi {{first_name}},


Thanks for checking out env0. Curious to learn what you think so far, given your role at {{company}}.

We typically work with {{!title}} who want to automate their Terraform, but are still finding it full of manual work, lack of auditability, or lack of consistency. Or maybe they’re trying to establish guardrails and governance with RBAC and OPA. And the most advanced are moving towards a self-service model to empower their teams to provision on-demand.

Is any of this happening in your day-to-day, or am I just rambling?


{{ sender.first_name }}


## 3 - Bridgebound

Re-engage previous deals. 


#### Gone Dark

Hello {{first_name}},

The reason for my email is because you had evaluated env0 a while back. Since then we've added these new features:

If you'll give me 15 mins, I can share with you how ZipCo cut deployment times in half. 

Either way, hope your week is going well.

Warm Regards, 
{{sender.first_name}}


#### Closed Lost

## 4 - Field / Events

#### Kubecon follow up

Thanks for stopping by our booth at KubeCon, {{first_name}}.

You had mentioned {{!personalize}}, and I promised we'd continue the conversation.

What does your calendar look like for a brief call?

{{sender.first_name}}


## 5 - Ancillary

#### Demo Confirmation (use if meeting was booked more than a week in advance)

Sub: Confirming for tomorrow
Hi {{contact.first_name}},

A friendly reminder about our meeting tomorrow at ( time}}. Ben, one of our IaC experts, and I are preparing now, and we're excited to share some insights with you. 

Warm regards,
{{sender.first_name}}


#### We Just Spoke

// use if spoke on the phone, but no meeting set

Sub: we just spoke /  Kevin from env0

Hi {{contact.first_name}},

It was a pleasure speaking with you, and happy to provide more info on what we're discussing on the call. 

We’re hosting some brief screen shares showing how companies like Paypal and MongoDB have been able to 

Here are a few resources I promised to send over:
Recommendations for Migrating from Terraform Cloud
Manual Approval Workflow using env0’s Custom Flow and API
Time to provide a Terraform Provider!
Provision and Manage Cloud Resources with IaC


( Since you're currently using X / based on our conversation }}, I think you'll find this super valuable.

Here are some available times:
( Mon 11/2 at 11:00 am or 2:00 pm EST // Tue 11/3 at 10:00 am EST ))

Feel free to suggest another time that works for you.

(I hope the information proves to be valuable. If your team ever wants to see these in action, I’d be happy to set up a call.)


#### Referral 

Sub: {{!referrer's name}} said we should speak

Hi {{first_name}},

Writing to you because I had a nice conversation with {{!referrer's name}}. We got around to discussing you and your role at {{!company}}, and they thought it'd be beneficial for us to speak. I promised {{!pronoun}} that I'd reach out to you.


What does your calendar look like for a brief call this week? Shouldn't take more than 7 minutes.


Warm Regards,
{{sender.first_name}}

#### No Show

Sub: Reschedule our appointment
Hi {{contact.first_name}}, 

We had a meeting set for ( DAY AND TIME OF MEETING IN FORMAT Today/yesterday/Friday at 2:00 pm EST )). I know things come up sometimes, so no big deal that we didn't get to speak. 

I was excited to show you how companies like Paypal and MongoDB automated their IaC and created a self-service catalogue for their ephemeral environments.

Do you have time to talk on  (weekdays_from_now 2) or  (weekdays_from_now 3)? Here are some available times:

 (weekdays_from_now 2) at 
 (weekdays_from_now 2) at 
 (weekdays_from_now 2) at 
 (weekdays_from_now 3) at
 (weekdays_from_now 3) at


Best,
{{sender.first_name}}


#### 5 min Reminder

// send 3-5 mins before meeting is set to occur

Sub: meeting now with env0

Hi {{contact.first_name}},

Wanted to make sure that finding our meeting link was the easiest part of your day: 

{{!zoom_link}}

See you shortly,
Kevin

#### 24hr invite not accepted

Sub: confirming you received the invite
Hi {{contact.first_name}},

It was great speaking with you yesterday. 

We scheduled a call for (Day/Date}} and I noticed you hadn't accepted the calendar invite. I just wanted to make sure it didn't go to spam and came through fine.

Did you receive it? If not, I can resend it to you.

In the meantime, we're doing our research on {{company}} and are looking forward to connecting. 

Talk soon,
{{sender.first_name}}

## 6 - Nurture

#### Onboarding Dropoff

Hi {{first_name}},


Noticed you were looking at env0 but didn't actually get to deploy! No need to look through the window, I can give you a hand setting up your infrastructure-as-code.

{{!gif}}

We've helped companies like Paypal and Virgin Media O2 deploy and manage infrastructure-as-code at scale. Happy to chat about how we can help you do the same.


Cheers,
{{ sender.first_name }}
env0




#### Previous Usage
Hi {{first_name}},


{{!personalize}}


I typically work with DevOps engineers who are looking to solve one of three problems caused by Infrastructure at scale.


1. Automating infra with a GitOps workflow
2. Enabling a developer self-service catalogue
3. Building developer friendly guardrails to suit governance/compliance requirements.


If you're working on any of these three initiatives, would you be open to a brief chat?


Cheers,


{{ sender.first_name }}
env0


## 6 - Nurture

### Various Nurture Content

#### Email - User Story

Subject: cut your merge time

Hi {{first_name}},

Had a chat with a customer (a Sr DevOps Engineer working in FinTech) and thought you might find this helpful:

“The plan and apply phase would take me about an hour and a half. That is a horrible user experience to commit a change, and then basically go get lunch. And by the time that’s done, I still have to wait for any kind of results of whether or not the plans calculated by those changes broke anything in any of the environments. [....]

I can unequivocally say that our time to merge was cut in half. I can’t really speak to how many errors env0 has been responsible for cutting… but it feels like it’s certainly helped a lot. It certainly has decreased the need for tribal knowledge over what has been applied to what environment.”

Does this resonate at all?

Warm Regards,
{{sender.first_name}}

#### Email - Video Demo

Hi {{first_name}},

Thought you’d find this quick 3min video on infrastructure-as-code useful:

Infrastructure as Code Automation Demo by env0

Warm Regards,
{{sender.first_name}}

#### Email - Case Study

Hi {{first_name}},


Stop me if you’ve heard this before. Have you ever committed a change, went out to get lunch, and by the time you got back, were still waiting for results of whether or not the plans calculated by those changes broke anything in any of the environments?
 
One of our users,Virgin Media O2, used env0 to solve this problem and shortened deployment times to less than ten minutes.

Cut their merge time in half
Reduced errors
Decreased the need for tribal knowledge (over what’s been applied to the environment)
 
See out the full case study here.
 
I’d love to grab 15 minutes to show you what we’ve built and how it can help. Let me know some times that work for you or if it’s easier please use this link to schedule time on my calendar.



Warm Regards,
{{sender.first_name}}



#### Email - eBook

Hi {{first_name}},


Want to get started in infrastructure-as-code? We put together a resource going over the pros and cons of each of the different frameworks (Terraform, Pulumi, Crossplane, Cloudformation, etc.)

Get the Infrastructure as Code 101 pdf here. Ungated, for your viewing pleasure.

Warm Regards,
{{sender.first_name}}


#### Email - Data Sheet

Hi {{first_name}},


Just the facts.

See the Data Sheet here.

Warm Regards,
{{sender.first_name}}

#### Email - Feature Highlight


sub: A new way to manage Terraform remote state / Read the Ultimate Guide on How to Implement a Remote Backend in 2023
 
Hello {{first_name}},
 
What if you didn’t have to go through the trouble of creating and maintaining a remote backend—including the backup, replication, high availability, encryption, and locking? With our new built-in remote backend, you can now use env0 to manage your state file in the same place where you run and deploy your Infrastructure.
 
Here's how they did it:
1.	One-Click Remote Backend: No need to set up or manage the underlying infrastructure.
2.	Configure-less remote state: No need to provide any configuration. Just deploy your code and we’ll automatically configure the backend. 
3.	Secure State Storage: Your state is encrypted and stored securely so only you can access
 
 
Read more about it in our Ultimate Guide to Managing Terraform Remote State (with bonus insights into remote backend best practices).
 

